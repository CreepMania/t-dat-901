const mysqlx = require('@mysql/xdevapi');

const config = {
  host     : 'mysql',
  user     : 'user',
  password : 'user',
  schema   : 'BigData'
};

const client = mysqlx.getClient(
  config,
  { pooling: { enabled: true, maxIdleTime: 30000, maxSize: 100, queueTimeout: 10000 } }
);

function getSession() {
  return client.getSession();
}

module.exports = getSession;
