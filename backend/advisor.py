#!/usr/bin/python

import sys
import requests
import json
import re
import urllib.parse
import os

API_URL = "http://127.0.0.1:9000/"

ERROR_CODES = {
    "NO_ERROR": 0,
    "TOO_FEW_ARGUMENTS": 1,
    "WRONG_COMMAND": 2,
    "TOO_FEW_ARGUMENTS_GET_USER_PROFILE": 3,
    "TOO_FEW_ARGUMENTS_GET_USER_RECOMMENDATION": 4,
    "API_REQUEST_FAILED": 5,
    "WRONG_ARGUMENT_TYPE": 6
}

CLIENT_PERSONALITY = [
    "DEPENSIER",
    "STANDARD",
    "ECONOME",
    "REGULIER",
    "PONCTUEL",
    "MULTI_FAMILLE",
    "MONO_FAMILLE"
]

TOP_BOUGHT_ARTICLE_SIZE = 3
TOP_CLIENT_SIZE_ARTICLE = 100
TOP_CLIENT_SIZE_SEGMENT = 300
ARTICLE_WEIGHT = 3
RECOMMENDATION_SIZE = 1
AVERAGE_POURCENT = 20
FREQUENCE_MONTH_QUANTITY = 3
NBR_OF_DIFFERENT_FAMILY = 3

SEGMENT_JUSTIFICATION = [
    "Si les paniers du client sont plus grand ou plus petit de X% par rapport au panier moyen. [DEPENSIER/STANDARD/ECONOME]",
    "Si le client a fait au moins 1 achat tous les X mois. [REGULIER/PONCTUEL]",
    "Si le client a fait des achats dans plus de X familles. [MULTI_FAMILLE/MONO_FAMILLE]"
]

SEGMENT_VARIABLE = [
    AVERAGE_POURCENT,
    FREQUENCE_MONTH_QUANTITY,
    NBR_OF_DIFFERENT_FAMILY
]

variablesSegment = [
    {
        "name": "Taille échantillon produit préféré",
        "value": TOP_BOUGHT_ARTICLE_SIZE
    },
    {
        "name": "Taille échantillon client se comportant de la même manière",
        "value": TOP_CLIENT_SIZE_SEGMENT
    },
    {
        "name": "Poids des articles dans les calculs",
        "value": ARTICLE_WEIGHT
    },
    {
        "name": "Nombre de produit à recommander",
        "value": RECOMMENDATION_SIZE
    }
]

variablesArticle = [
    {
        "name": "Taille échantillon produit préféré",
        "value": TOP_BOUGHT_ARTICLE_SIZE
    },
    {
        "name": "Taille échantillon client achetant les même produits",
        "value": TOP_CLIENT_SIZE_ARTICLE
    },
    {
        "name": "Poids des articles dans les calculs",
        "value": ARTICLE_WEIGHT
    },
    {
        "name": "Nombre de produit à recommander",
        "value": RECOMMENDATION_SIZE
    }
]

def getTop(itemDict, size):
    bestKeyDict = list()
    for item in itemDict:
        if len(bestKeyDict) < size:
            bestKeyDict.append(item)
        else:
            delta = 0
            badKey = ""
            for key in bestKeyDict:
                if itemDict[item] > itemDict[key]:
                    if (itemDict[item] - itemDict[key]) > delta:
                        delta = itemDict[item] - itemDict[key]
                        badKey = key
            if delta != 0:
                bestKeyDict.remove(badKey)
                bestKeyDict.append(item)
    
    return bestKeyDict

def getMostBought(userJson, key):
    multimap = json.loads(userJson)
    nameList = dict()
    for line in multimap:
        if not line["name"] in nameList:
            nameList[line["name"]] = line["qte"]
        else:
            nameList[line["name"]] += line["qte"]
    return getTop(nameList, TOP_BOUGHT_ARTICLE_SIZE)

def stringToInt(string):
    return re.sub("[^0-9]", "", string)

def printJson(string):
    print(json.dumps(string, indent=4), flush=True)

def printJsonError(string):
    printJson({"error": string})

def requestURL(string):
    try:
        response = requests.get(url = string).text
    except:
        printJsonError("Request '" + string + "' failed.")
        exit(ERROR_CODES.get("API_REQUEST_FAILED"))
    return response

def checkArguments():
    if len(sys.argv) != 3:
        printJsonError("Too few arguments, GET_USER_RECOMMENDATION command needs the id of the user.")
        exit(ERROR_CODES.get("TOO_FEW_ARGUMENTS_GET_USER_RECOMMENDATION"))
    userJson = {}
    userId = sys.argv[2]
    if not userId.isdigit():
        printJsonError("ID arguments needs to be a number.")
        exit(ERROR_CODES.get("WRONG_ARGUMENT_TYPE"))
    return userId

def getTopClientList(topBoughtArticle, size):
    clientDict = dict()
    for article in topBoughtArticle:
        clientList = json.loads(requestURL(API_URL + "article/clients?article=" + urllib.parse.quote(article, safe='')))
        for clientId in clientList:
            if clientId in clientDict:
                clientDict[clientId] += ARTICLE_WEIGHT
            else:
                clientDict[clientId] = ARTICLE_WEIGHT
    return getTop(clientDict, size)

def getTopArticleFromUserId(userId):
    userPurchase = requestURL(API_URL + "users/" + userId + "/articles/list")
    return getMostBought(userPurchase, "name")

def getClientExpenses(clientId, globalAverageTicket):
    clientAverageTicket = json.loads(requestURL(API_URL + "users/" + clientId + "/cart/average"))[0]
    lowLimit = (globalAverageTicket * (100 - AVERAGE_POURCENT)) / 100
    hightLimit = (globalAverageTicket * (100 + AVERAGE_POURCENT)) / 100
    if clientAverageTicket < lowLimit:
        return CLIENT_PERSONALITY[2]
    elif clientAverageTicket > hightLimit:
        return CLIENT_PERSONALITY[0]
    else:
        return CLIENT_PERSONALITY[1]

def getClientFrequency(clientId):
    clientTiquetFrequency = json.loads(requestURL(API_URL + "users/" + clientId + "/tickets/monthly"))
    for i in range(0, int(len(clientTiquetFrequency) / FREQUENCE_MONTH_QUANTITY)):
        nbrOfTicket = 0
        for j in range(0, FREQUENCE_MONTH_QUANTITY):
            nbrOfTicket += clientTiquetFrequency[(i * FREQUENCE_MONTH_QUANTITY) + j]
        if nbrOfTicket == 0:
            return CLIENT_PERSONALITY[4]
    return CLIENT_PERSONALITY[3]

def getClientFamilyType(clientId):
    clientBuy = json.loads(requestURL(API_URL + "users/" + clientId + "/articles/list"))
    family = list()
    for line in clientBuy:
        if not line["family"] in family:
            family.append(line["family"])
    if len(family) > NBR_OF_DIFFERENT_FAMILY:
        return CLIENT_PERSONALITY[5]
    else:
        return CLIENT_PERSONALITY[6]
    return

def segmentClient(clientId):
    clientSegment = list()
    globalAverageTicket = json.loads(requestURL(API_URL + "users/cart/average"))[0]
    if globalAverageTicket != None:
        clientSegment.append(getClientExpenses(clientId, globalAverageTicket))
    else:
        clientSegment.append(CLIENT_PERSONALITY[1])
    clientSegment.append(getClientFrequency(clientId))
    clientSegment.append(getClientFamilyType(clientId))
    return clientSegment

def buildRaisonArray(userSegment):
    return [{"segment": segment, "description": SEGMENT_JUSTIFICATION[idx].replace("X", str(SEGMENT_VARIABLE[idx]))} for idx, segment in enumerate(userSegment)]

def getUserRecommendationSegmentBased():
    userId = checkArguments()
    userSegment = segmentClient(userId)
    topBoughtArticle = getTopArticleFromUserId(userId)
    topClientList = getTopClientList(topBoughtArticle, TOP_CLIENT_SIZE_SEGMENT)
    segmentClientList = list()
    for clientId in topClientList:
        clientSegment = segmentClient(str(clientId))
        if clientSegment == userSegment:
            segmentClientList.append(clientId)
    topArticleDict = dict()
    globalAverageTicket = json.loads(requestURL(API_URL + "users/cart/average"))[0]
    for clientId in segmentClientList:
        topArticle = getTopArticleFromUserId(str(clientId))
        for article in topArticle:
            if article in topArticleDict:
                topArticleDict[article] += 1
            else:
                topArticleDict[article] = 1
    for article in topBoughtArticle:
        if article in topArticleDict:
            topArticleDict.pop(article)
    recommendation = getTop(topArticleDict, RECOMMENDATION_SIZE)
    raisonArray = buildRaisonArray(userSegment)
    printJson({"recommendation": recommendation, "raison": raisonArray, "variables": variablesSegment})

def getUserRecommendationArticleBased():
    userId = checkArguments()
    topBoughtArticle = getTopArticleFromUserId(userId)
    topClientList = getTopClientList(topBoughtArticle, TOP_CLIENT_SIZE_ARTICLE)
    topArticleDict = dict()
    for clientId in topClientList:
        topArticle = getTopArticleFromUserId(str(clientId))
        for article in topArticle:
            if article in topArticleDict:
                topArticleDict[article] += 1
            else:
                topArticleDict[article] = 1
    for article in topBoughtArticle:
        if article in topArticleDict:
            topArticleDict.pop(article)
    recommendation = getTop(topArticleDict, RECOMMENDATION_SIZE)
    printJson({"recommendation": recommendation, "raisons": topBoughtArticle, "variables": variablesArticle})

def getCommandList():
    printJson({"commands": [*COMMANDS.keys()]})

COMMANDS = {
    "GET_COMMAND_LIST": getCommandList,
    "GET_USER_RECOMMENDATION_ARTICLE_BASED": getUserRecommendationArticleBased,
    "GET_USER_RECOMMENDATION_SEGMENT_BASED": getUserRecommendationSegmentBased
    }

if len(sys.argv) < 2:
    printJsonError("Too few arguments (2 at least).")
    exit(ERROR_CODES.get("TOO_FEW_ARGUMENTS"))

try:
    COMMANDS[sys.argv[1]]()
    exit(0)
except Exception as e:
    printJsonError("Unkown command '{}'. To get the command list try 'GET_COMMAND_LIST'.".format(sys.argv[1]))
    exit(ERROR_CODES.get("WRONG_COMMAND"))
exit(ERROR_CODES.get("NO_ERROR"))
