var express = require('express');
var router = express.Router();
const { COLOR_FAMILIES } = require('../const');

const db = require("../db")

var SEGMENTATION = {}
var SOLD_FAMILIES = {}
var REVENUE_FAMILIES = {}

// init segmentation
db().then(async session => {
  var result = await session.sql(
    `SELECT ((COUNT(*) / 2734840) * 100) AS Proportion, DataTable.PriceRange
    FROM (
        SELECT IF(CEILING(SUM(a.NetPrice)) >= 50, 50, CEILING(SUM(a.NetPrice))) AS PriceRange
        FROM Articles a
        INNER JOIN Articles_Tickets a_t
        ON a.Id = a_t.ArticleId
        GROUP BY a_t.TicketId
    )DataTable
    GROUP BY DataTable.PriceRange
    HAVING Proportion > 0.001
    ORDER BY DataTable.PriceRange`).execute();

  var data = result.fetchAll();
  var obj = [];
  data.forEach(element => {
    obj.push({ proportion: element[0], price: element[1] })
  });
  session.close();
  SEGMENTATION = obj
})

db().then(async session => {
  var result = await session.sql(
    `SELECT f.Name, COUNT(a.Id)
    FROM Families f
    INNER JOIN Categories c ON f.Id = c.FamilyId
    INNER JOIN Articles a ON c.Id = a.CategoryId
    INNER JOIN Articles_Tickets a_t ON a.Id = a_t.ArticleId
    GROUP BY f.Name
    ORDER BY 2 DESC`).execute();

  var data = result.fetchAll();
  var obj = [];
  data.forEach(element => {
    obj.push({ name: element[0], value: element[1], color: COLOR_FAMILIES[element[0]] })
  });
  SOLD_FAMILIES = obj
  session.close();
})

db().then(async session => {
  var result = await session.sql(
    `SELECT f.Name, SUM(a.NetPrice)
    FROM Families f
    INNER JOIN Categories c ON f.Id = c.FamilyId
    INNER JOIN Articles a ON c.Id = a.CategoryId
    INNER JOIN Articles_Tickets a_t ON a.Id = a_t.ArticleId
    GROUP BY f.Name
    ORDER BY 2 DESC`).execute();

  var data = result.fetchAll();
  var obj = [];
  data.forEach(element => {
    obj.push({ name: element[0], value: element[1], color: COLOR_FAMILIES[element[0]] })
  });
  REVENUE_FAMILIES = obj
  session.close();
})

/* GET nb sold articles */
router.get('/articles/count', function (req, res, next) {
  db().then(async session => {
    var result = await session.sql('SELECT COUNT(*) FROM  Articles_Tickets').execute();
    res.send(result.fetchOne());
    session.close();

  })
    .catch(err => next(err));
});

/* GET Top 10 sold articles */
router.get('/articles/top10', function (req, res, next) {
  db().then(async session => {
    var result = await session.sql(
      `SELECT f.Name 'Family', a.Name 'Article', (COUNT(a.Id) / 7245237 ) * 100 'Proportion', a.NetPrice 'Price'
      FROM Articles_Tickets a_t
      INNER JOIN Articles a ON (a.Id = a_t.ArticleId)
      INNER JOIN Categories c ON a.CategoryId = c.Id
      INNER JOIN Families f ON c.FamilyId = f.Id
      GROUP BY a.Id
      ORDER BY COUNT(a.Id) DESC
      LIMIT 10`).execute();
    var data = result.fetchAll();
    var obj = [];
    for (let i = 0; i < data.length; i++) {
      obj.push({ position: i + 1, family: data[i][0], name: data[i][1], proportion: data[i][2], price: data[i][3] })
    };
    res.send(obj);
    session.close();

  })
    .catch(err => next(err));
});

/* GET number of articles by family */
router.get('/articles/families', function (req, res, next) {
  db().then(async session => {
    var result = await session.sql(
      `SELECT f.Name, COUNT(a.Id)
      FROM Families f
      INNER JOIN Categories c ON f.Id = c.FamilyId
      INNER JOIN Articles a ON c.Id = a.CategoryId
      GROUP BY f.Name
      ORDER BY 2 DESC`).execute();
    var data = result.fetchAll();
    var obj = [];
    data.forEach(element => {
      obj.push({ name: element[0], value: element[1] })
    });
    res.send(obj);
    session.close();

  })
    .catch(err => next(err));
});

/* GET number of articles SOLD by family */
router.get('/articles/sold/families', function (req, res, next) {
  res.send(SOLD_FAMILIES)
});

/* GET total revenue by family */
router.get('/articles/revenue/families', function (req, res, next) {
  res.send(REVENUE_FAMILIES)
});

/* GET number of clients */
router.get('/clients/count', function (req, res, next) {
  db().then(async session => {
    var result = await session.sql(
      `SELECT COUNT(DISTINCT t.ClientId)
      FROM Tickets t`).execute();
    res.send(result.fetchOne());
    session.close();
  }
  )
});

/* GET total revenue */
router.get('/revenue/total', function (req, res, next) {
  db().then(async session => {
    var result = await session.sql(
      `SELECT SUM(a.NetPrice) 
      FROM Articles_Tickets a_t
      INNER JOIN Articles a
      ON a.Id = a_t.ArticleId`).execute();
    res.send(result.fetchOne());
    session.close();

  })
    .catch(err => next(err));
});

/* GET number of tickets by month */
const MONTHS = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
router.get('/tickets/monthly', function (req, res, next) {
  db().then(async session => {
    var result = await session.sql(
      `SELECT COUNT(t.Id), t.Month
      FROM Tickets t
      GROUP BY t.Month
      ORDER BY t.Month`)
      .execute();
    var data = result.fetchAll();
    var obj = [];

    MONTHS.forEach(month => {
      var monthValue = data.filter(v => v[1] == month);
      if (monthValue.length != 0) {
        obj.push(monthValue[0][0])
      } else {
        obj.push(0);
      }
    });

    res.send(obj);
    session.close();

  })
    .catch(err => next(err));
});

/* GET proportion in % of ticket's total from 0 to 50$ */
router.get('/tickets/segmentation', function (req, res, next) {
  res.send(SEGMENTATION);
});

/* GET list of clients that bought a specific article */
router.get('/article/clients', function (req, res, next) {
  db().then(async session => {
    var result = await session.sql(`CALL GetClientIdsWithArticleName(?)`)
      .bind(req.query.article)
      .execute();

    res.send(result.fetchAll().map(v => v[0]));
    return session.close();
  })
    .catch(err => next(err));
});

module.exports = router;
