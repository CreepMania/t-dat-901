var express = require('express');
var router = express.Router();

const { COLOR_FAMILIES } = require('../const')

const db = require('../db')

var TICKET_AVERAGE = {}
var CART_AVERAGE = {}
var TOTAL_AVERAGE = 0

db().then(async session => {
  var result = await session.sql(
    `SELECT AVG(co.nb)
     FROM (SELECT COUNT(t.Id) nb
      FROM Tickets t
      GROUP BY t.ClientId) co`)
    .execute();
  TICKET_AVERAGE = result.fetchOne()
  session.close();
})
.catch(err => console.error(err))

db().then(async session => {
  var result = await session.sql(
    `
    SELECT AVG(Cart.Price)
    FROM (
        SELECT SUM(a.NetPrice) AS Price
        FROM Articles_Tickets a_t
        INNER JOIN Articles a ON (a.Id = a_t.ArticleId)
        GROUP BY a_t.TicketId
    ) Cart`)
    .execute();
  CART_AVERAGE = result.fetchOne()
  session.close();
})
.catch(err => console.error(err))


db().then(async session => {
  var result = await session.sql(
    `SELECT AVG(Purchase.Total)
    FROM (
      SELECT SUM(a.NetPrice) Total
        FROM Tickets t
        INNER JOIN Articles_Tickets a_t
        ON t.Id = a_t.TicketId
        INNER JOIN Articles a
        ON a.Id = a_t.ArticleId
        GROUP BY t.ClientId
    )Purchase`)
    .execute();
  TOTAL_AVERAGE = result.fetchOne()
  session.close();
})
.catch(err => console.error(err));

router.get('/:_id/tickets/count', function (req, res, next) {
  db().then(async session => {
    var result = await session.sql(`SELECT COUNT(*) 
                                  FROM Tickets t
                                  WHERE ClientId = (?)`)
      .bind(req.params._id)
      .execute();
    res.send(result.fetchOne());
    session.close();

  })
  .catch(err => next(err));
});

router.get('/tickets/average', function (req, res, next) {
  res.send(TICKET_AVERAGE)
});

router.get('/:_id/cart/average', function (req, res, next) {
  db().then(async session => {
    var result = await session.sql(
      `SELECT AVG(Cart.Price)
      FROM (
          SELECT SUM(a.NetPrice) AS Price
          FROM Tickets t
          INNER JOIN Articles_Tickets a_t ON (t.Id = a_t.TicketId)
          INNER JOIN Articles a ON (a.Id = a_t.ArticleId)
          WHERE t.ClientId = ?
          GROUP BY t.Id
      ) Cart`)
      .bind(req.params._id)
      .execute();
    res.send(result.fetchOne());
    session.close();

  })
  .catch(err => next(err));
});

router.get('/cart/average', function (req, res, next) {
  res.send(CART_AVERAGE)
});

router.get('/:_id/total', function (req, res, next) {
  db().then(async session => {
    var result = await session.sql(
      `SELECT SUM(a.NetPrice)
      FROM Tickets t
      INNER JOIN Articles_Tickets a_t
      ON t.Id = a_t.TicketId AND t.ClientId = ?
      INNER JOIN Articles a
      ON a.Id = a_t.ArticleId`)
      .bind(req.params._id)
      .execute();
    res.send(result.fetchOne());
    session.close();

  })
  .catch(err => next(err));
});

router.get('/total', function (req, res, next) {
  res.send(TOTAL_AVERAGE)
});


const MONTHS = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
router.get('/:_id/tickets/monthly', function (req, res, next) {
  db().then(async session => {
    var result = await session.sql(
      `SELECT COUNT(t.Id), t.Month
      FROM Tickets t
      WHERE t.ClientId = ?
      GROUP BY t.Month
      ORDER BY t.Month`)
      .bind(req.params._id)
      .execute();
    var data = result.fetchAll();
    var obj = [];

    MONTHS.forEach(month => {
      var monthValue = data.filter(v => v[1] == month);
      if (monthValue.length != 0) {
        obj.push(monthValue[0][0])
      } else {
        obj.push(0);
      }
    });

    res.send(obj);
    session.close();

  })
  .catch(err => next(err));
});

router.get('/:_id/articles/sold/families', function (req, res, next) {
  db().then(async session => {
    var result = await session.sql(
      `SELECT f.Name, COUNT(a.Id)
      FROM Families f
      INNER JOIN Categories c ON f.Id = c.FamilyId
      INNER JOIN Articles a ON c.Id = a.CategoryId
      INNER JOIN Articles_Tickets a_t ON a.Id = a_t.ArticleId
      INNER JOIN Tickets t ON t.Id = a_t.TicketId AND t.ClientId = ?
      GROUP BY f.Name
      ORDER BY 2 DESC`)
      .bind(req.params._id)
      .execute();
    var data = result.fetchAll();
    var obj = [];
    data.forEach(element => {
      obj.push({name: element[0], value: element[1], color: COLOR_FAMILIES[element[0]]})
    });
    res.send(obj);
    session.close();

  })
  .catch(err => next(err));
});

router.get('/:_id/articles/revenue/families', function (req, res, next) {
  db().then(async session => {
    var result = await session.sql(
      `SELECT f.Name, SUM(a.NetPrice), ROUND((SUM(a.NetPrice) / ( SELECT SUM(a.NetPrice)
                                                            FROM Tickets t
                                                            INNER JOIN Articles_Tickets a_t
                                                            ON t.Id = a_t.TicketId AND t.ClientId = ?
                                                            INNER JOIN Articles a
                                                            ON a.Id = a_t.ArticleId)) * 100, 2) AS Perc
      FROM Families f
      INNER JOIN Categories c ON f.Id = c.FamilyId
      INNER JOIN Articles a ON c.Id = a.CategoryId
      INNER JOIN Articles_Tickets a_t ON a.Id = a_t.ArticleId
      INNER JOIN Tickets t ON a_t.TicketId = t.Id AND t.ClientId = ?
      GROUP BY f.Name
      ORDER BY 2 DESC`)
      .bind(req.params._id, req.params._id)
      .execute();
      var data = result.fetchAll();
      var obj = [];
      data.forEach(element => {
        obj.push({name: element[0], value: element[1], perc: element[2], color: COLOR_FAMILIES[element[0]]})
      });
      res.send(obj);
      session.close();

    })
    .catch(err => next(err));
});

router.get('/:_id/articles/list', function (req, res, next) {
  db().then(async session => {
    var result = await session.sql(`CALL GetArticleListForClient(?)`)
      .bind(req.params._id)
      .execute();
    var data = result.fetchAll();
    var obj = [];

    for(var i = 0; i < data.length; ++i) {
      obj.push({position: i+1, name: data[i][0], family: data[i][2], price: data[i][1], qte: data[i][5]})
    }

    res.send(obj);
    return session.close();
  })
  .catch(err => next(err));
});

const { spawn } = require("child_process");

router.get('/:_id/segmentrecommandation', function (req, res, next) {
  new Promise((resolve, reject) => {
    var dataToSend;
    const python = spawn('python3', ['./advisor.py', 'GET_USER_RECOMMENDATION_SEGMENT_BASED', req.params._id]);

    python.stdout.on('data', function (data) {
      console.log('Pipe data from python script ...');
      dataToSend = JSON.parse(data.toString());
    });

    python.on('close', (code) => {
      if (code != 0) {
        console.log(`child process close all stdio with code ${code}`);
        return reject(dataToSend)
      }
      return resolve(dataToSend)
    })

    python.on('exit', (code) => {
      if (code != 0) {
        console.log(`child process close all stdio with code ${code}`);
        return reject(dataToSend)
      }
    })
  })
  .then(r => res.json(r))
  .catch(e => {console.error(e); next(e)})
    
});

router.get('/:_id/articlerecommandation', function (req, res, next) {
  new Promise((resolve, reject) => {
    var dataToSend;
    const python = spawn('python3', ['./advisor.py', 'GET_USER_RECOMMENDATION_ARTICLE_BASED', req.params._id]);

    python.stdout.on('data', function (data) {
      console.log('Pipe data from python script ...');
      dataToSend = JSON.parse(data.toString());
    });

    python.on('close', (code) => {
      if (code != 0) {
        console.log(`child process close all stdio with code ${code}`);
        return reject(dataToSend)
      }
      return resolve(dataToSend)
    })

    python.on('exit', (code) => {
      if (code != 0) {
        console.log(`child process close all stdio with code ${code}`);
        return reject(dataToSend)
      }
    })
  })
  .then(r => res.json(r))
  .catch(e => {console.error(e); next(e)})
    
});

router.get('/check/:_id', function (req, res, next) {
  db().then(async session => {
    var result = await session.sql(`SELECT COUNT(*) FROM Tickets WHERE ClientId = (?)`)
      .bind(req.params._id)
      .execute();
    var data = result.fetchOne();
    session.close();

    if (data == 0)
      return res.send(false)
    else return res.send(true);

  })
  .catch(err => next(err));
});

module.exports = router;
