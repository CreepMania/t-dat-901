import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { environment } from './../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class GlobalStatsService {

  constructor(private http: HttpClient) { }

  url = environment.apiUrl

  getTotalSoldArticles(): Observable<number> {
    return this.http.get<number>(this.url + "/articles/count")
  }

  getTotalClients(): Observable<number> {
    return this.http.get<number>(this.url + "/clients/count")
  }

  getTotalRevenue(): Observable<number> {
    return this.http.get<number>(this.url + "/revenue/total")
  }

  getTopTenSoldArticles(): Observable<any[]> {
    return this.http.get<any[]>(this.url + "/articles/top10");
  }

  getMonthlyTickets(): Observable<number[]> {
    return this.http.get<number[]>(this.url + "/tickets/monthly")
  }

  getNumberOfArticlesByFamily(): Observable<{name, value}[]> {
    return this.http.get<{name, value}[]>(this.url + "/articles/families")
  }

  getSoldUnitsByFamily(): Observable<{name, value, color}[]> {
    return this.http.get<{name, value, color}[]>(this.url + "/articles/sold/families")
  }

  getRevenueByFamily(): Observable<{name, value, color}[]> {
    return this.http.get<{name, value, color}[]>(this.url + "/articles/revenue/families");
  }

  getTicketSegmentation(): Observable<{proportion, price}[]> {
    return this.http.get<{proportion, price}[]>(this.url + "/tickets/segmentation");
  }
}
