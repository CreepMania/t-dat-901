import { Component, OnInit } from '@angular/core';
import { GlobalStatsService } from '../global-stats.service';



const FAMILIES = ['HYGIENE', 'SOINS DU VISAGE', 'PARFUMAGE', 'SOINS DU CORPS', 'MAQUILLAGE', 'CAPILLAIRE', 'SOLAIRES', 'MULTI', 'SANTE NATURELLE'];
const MONTHS = ['Janvier', 'Fevrier', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Aout', 'Septembre', 'Octobre', 'Novembre', 'Décembre'];
const PRICE_SEGMENTATION = [ "1" ,"2" ,"3" ,"4" ,"5" ,"6" ,"7" ,"8" ,"9" ,"10" ,"11" ,"12" ,"13" ,"14" ,"15" ,"16" ,"17" ,"18" ,"19" ,"20" ,"21" ,"22" ,"23" ,"24" ,"25" ,"26" ,"27" ,"28" ,"29" ,"30" ,"31" ,"32" ,"33" ,"34" ,"35" ,"36" ,"37" ,"38" ,"39" ,"40" ,"41" ,"42" ,"43" ,"44" ,"45" ,"46" ,"47" ,"48" ,"49" ,"50"]
const COLORS = ['#90f997', '#9790f9', '#99e5e5', '#D255F5', '#F4A459', '#B6C851', '#BD3A40', '#01083D', '#165552']
const COLOR_FAMILIES = {}
FAMILIES.map((v, i) => COLOR_FAMILIES[v] = COLORS[i])

@Component({
  selector: 'app-global-stats',
  templateUrl: './global-stats.component.html',
  styleUrls: ['./global-stats.component.scss']
})
export class GlobalStatsComponent implements OnInit {
  totalSoldArticles: String;
  totalClients: String;
  totalRevenue: String;
  displayedColumns: string[] = ['position', 'name', 'family', 'price', 'proportion'];
  dataSource: any[];

  ticketBarChartDataPoints: any[];
  familyBarChartDataPoints: any[];
  unitSoldByFamilyPieChartDataPoints: any[];
  profitByFamilyPieChartDataPoints: any[];
  ticketsSegmentationLineBarDataPoints: any[];

  // Répartition achats par mois
  public barChartOptions = {
    responsive: true,
    maintainAspectRatio: false,
    scales: {
      xAxes: [{ display: true }],
      yAxes: [{ gridLines: { display:false }, ticks: { beginAtZero: true }}],
    }, 
  };  
  public barChartLabels = MONTHS;
  public barChartType= 'bar';
  public barChartData = [
    {data: [], label: 'Ventes'},
  ];

  public familyBarChartOptions = {
    responsive: true,
    maintainAspectRatio: false,
    scales: {
      xAxes: [{ gridLines: { display:false } }],
      yAxes: [{ gridLines: { display:false }, ticks: { beginAtZero: true }}],
    }, 
  };  
  public familyBarChartLabels = FAMILIES;
  public familyBarChartType= 'bar';
  public familyBarChartData = [
    {data: [], label: 'Ventes' },
  ];

  public pieChartOptions = {
    responsive: true,
    maintainAspectRatio: false,
  };  
  public pieChartLabels = FAMILIES;
  public pieChartType = 'pie';
  public pieChartData = [
    {data: [], backgroundColor: [] },
  ];

  public salesPieChartOptions = {
    responsive: true,
    maintainAspectRatio: false,
  };  
  public salesPieChartLabels = FAMILIES;
  public salesPieChartType = 'pie';
  public salesPieChartData = [
    {data: [], label: 'Ventes', backgroundColor: [] },
  ];

  public lineChartOptions = {
    responsive: true,
    maintainAspectRatio: false,
    scales: {
      xAxes: [{ display:true }],
      yAxes: [{ gridLines: { display:false }, ticks: { beginAtZero: true }}],
    }, 
  };
  public lineChartLabels = PRICE_SEGMENTATION;
  public lineChartType= 'line';
  public lineChartData = [
    {data: [], label: '%'},
  ];

  constructor(private statService: GlobalStatsService) { }

  ngOnInit(): void {
    this.getSoldArticles();
    this.getTotalClients();
    this.getTotalRevenue();
    this.getTopTenSoldArticles();
    this.getMonthlyTickets();
    this.getNumberOfArticlesByFamily();
    this.getSoldUnitsByFamily();
    this.getRevenueByFamily();
    this.getTicketSegmentation();
  }

  getSoldArticles() {
    this.statService.getTotalSoldArticles().subscribe(res => this.totalSoldArticles = res.toLocaleString());
  }

  getTotalClients() {
    this.statService.getTotalClients().subscribe(res => this.totalClients = res.toLocaleString());
  }

  getTotalRevenue() {
    this.statService.getTotalRevenue().subscribe(res => this.totalRevenue = res.toLocaleString());
  }

  getTopTenSoldArticles() {
    this.statService.getTopTenSoldArticles().subscribe(res => this.dataSource = res);
  }

  getMonthlyTickets() {
    this.statService.getMonthlyTickets().subscribe(res => this.barChartData[0].data = res);
  }

  getNumberOfArticlesByFamily() {
    this.statService.getNumberOfArticlesByFamily().subscribe(res => {
      this.familyBarChartLabels = res.map(v => v.name)
      this.familyBarChartData[0].data = res.map(v => v.value)
    });
  }

  getSoldUnitsByFamily() {
    this.statService.getSoldUnitsByFamily().subscribe(res => {
      this.pieChartLabels = res.map(v => v.name)
      this.pieChartData[0].backgroundColor = res.map(v => v.color)
      this.pieChartData[0].data = res.map(v => v.value)
    });
  }

  getRevenueByFamily() {
    this.statService.getRevenueByFamily().subscribe(res => {
      this.salesPieChartLabels = res.map(v => v.name)
      this.salesPieChartData[0].backgroundColor = res.map(v => v.color)
      this.salesPieChartData[0].data = res.map(v => v.value)
    });
  }

  getTicketSegmentation() {
    this.statService.getTicketSegmentation().subscribe(res => {
      this.lineChartLabels = res.map(v => v.price)
      this.lineChartData[0].data = res.map(v => v.proportion)
    });
  }
  
}
