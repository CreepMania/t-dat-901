import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from './../environments/environment';

export interface IRecommenderObject {
  recommendation: string[],
  raison: {segment, description}[],
  variables: {name, value}[]
}

@Injectable({
  providedIn: 'root'
})
export class UserStatsService {
  url = environment.apiUrl + '/users'


  constructor(private http: HttpClient) { }

  getTicketAmount(userId: number): Observable<number> {
    return this.http.get<number>(this.url + '/' + userId + "/tickets/count");
  }

  getAverageTicketAmount(): Observable<number> {
    return this.http.get<number>(this.url + "/tickets/average");
  }

  getClientAverageCartTotal(userId: number): Observable<number> {
    return this.http.get<number>(this.url + '/' + userId + "/cart/average");
  }

  getAverageCartTotal(): Observable<number> {
    return this.http.get<number>(this.url + "/cart/average");
  }

  getClientTotal(userId: number): Observable<number> {
    return this.http.get<number>(this.url + '/' + userId + "/total");
  }

  getAverageTotal(): Observable<number> {
    return this.http.get<number>(this.url + "/total");
  }

  getClientMonthlyTickets(userId: number): Observable<number[]> {
    return this.http.get<number[]>(this.url +  '/' + userId + "/tickets/monthly");
  }

  getNumberBoughtByFamily(userId: number): Observable<{name, value, color}[]> {
    return this.http.get<{name, value, color}[]>(this.url + '/' + userId + "/articles/sold/families");
  }

  getTotalByFamily(userId: number): Observable<{name, value, color, perc}[]> {
    return this.http.get<{name, value, color, perc}[]>(this.url + '/' + userId + "/articles/revenue/families");
  }

  getArticlesBought(userId: number): Observable<Object[]> {
    return this.http.get<Object[]>(this.url + '/' + userId + "/articles/list");
  }

  getSegmentRecommandation(userId: number): Observable<IRecommenderObject> {
    return this.http.get<IRecommenderObject>(this.url + '/' + userId + "/segmentrecommandation");
  }

  getArticleRecommandation(userId: number): Observable<{recommendation, variables, raisons}> {
    return this.http.get<{recommendation, variables, raisons}>(this.url + '/' + userId + "/articlerecommandation");
  }

  getUserProfile(userId: number): Observable<Object> {
    return null;
    // return this.http.get<Object>(this.url + '/' + userId + "/profile");
  }

  checkUser(userId: number): Observable<boolean> {
    return this.http.get<boolean>(this.url + '/check/' + userId);
  }
}
