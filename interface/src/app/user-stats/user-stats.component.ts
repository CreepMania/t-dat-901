import { Component, OnInit } from '@angular/core';
import { GlobalStatsService } from '../global-stats.service';
import { UserStatsService } from '../user-stats.service';

import * as pluginAnnotations from 'chartjs-plugin-annotation';
import { AbstractControl, AsyncValidatorFn, FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

const FAMILIES = ['Hygiène', 'Soins du visage', 'Parfumage', 'Soins du corps', 'Maquillage', 'Capillaires', 'Solaires', 'Multi', 'Santé naturelle'];
const MONTHS = ['Janvier', 'Fevrier', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Aout', 'Septembre', 'Octobre', 'Novembre', 'Décembre'];
const PRICE_SEGMENTATION = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50"]

export class UserIdvalidator {
  static createValidator(userService: UserStatsService): AsyncValidatorFn {
    return (control: AbstractControl): Observable<ValidationErrors> => {
      return userService.checkUser(control.value).pipe(
        map((result: boolean) => result ? null : { invalidAsync: true })
      );
    };
  }
}

@Component({
  selector: 'app-user-stats',
  templateUrl: './user-stats.component.html',
  styleUrls: ['./user-stats.component.scss']
})
export class UserStatsComponent implements OnInit {
  userIdGroup: FormGroup;

  totalTickets: string = "";
  totalClients: String;
  totalRevenue: String;
  displayedColumns: string[] = ['position', 'name', 'family', 'price', 'qte'];
  listArticles: any[];
  public userPlugins = [pluginAnnotations]


  ticketBarChartDataPoints: any[];
  familyBarChartDataPoints: any[];
  unitSoldByFamilyPieChartDataPoints: any[];
  profitByFamilyPieChartDataPoints: any[];
  ticketsSegmentationLineBarDataPoints: any[];

  // Répartition achats par mois
  public barChartOptions = {
    responsive: true,
    maintainAspectRatio: false,
    scales: {
      xAxes: [{ display: true }],
      yAxes: [{ gridLines: { display: false }, ticks: { beginAtZero: true, stepSize: 1 } }],
    },
  };
  public barChartLabels = MONTHS;
  public barChartType = 'bar';
  public barChartData = [
    { data: [], label: 'Ventes' },
  ];

  public familyBarChartOptions = {
    responsive: true,
    maintainAspectRatio: false,
    scales: {
      xAxes: [{ gridLines: { display: false } }],
      yAxes: [{ gridLines: { display: false }, ticks: { beginAtZero: true } }],
    },
  };
  public familyBarChartLabels = FAMILIES;
  public familyBarChartType = 'bar';
  public familyBarChartData = [
    { data: [], label: 'Ventes' },
  ];

  public pieChartOptions = {
    responsive: true,
    maintainAspectRatio: false
  };
  public pieChartLabels = FAMILIES;
  public pieChartType = 'pie';
  public pieChartData = [
    { data: [], backgroundColor: [] },
  ];

  public salesPieChartOptions = {
    responsive: true,
    maintainAspectRatio: false
  };
  public salesPieChartLabels = FAMILIES;
  public salesPieChartType = 'pie';
  public salesPieChartData = [
    { data: [], label: 'Ventes', backgroundColor: [] },
  ];

  public clientTotal: string = "";

  public lineChartOptions = {
    responsive: true,
    maintainAspectRatio: false,
    scales: {
      xAxes: [{ display: true }],
      yAxes: [{ gridLines: { display: false }, ticks: { beginAtZero: true } }],
    },
    annotation: {
      annotations: [
        {
          type: 'line',
          mode: 'vertical',
          // drawTime: 'afterDatasetsDraw',
          scaleID: 'x-axis-0',
          value: '0',
          borderColor: 'orange',
          borderWidth: 2,
          label: {
            enabled: false,
            fontColor: 'orange',
            content: 'User'
          }
        }
      ],
    },
  };
  public lineChartLabels = PRICE_SEGMENTATION;
  public lineChartType = 'line';
  public lineChartData = [
    { data: [], label: '%' },
  ];

  //990290245
  public userId: number;
  public averageTicketNumber: String;
  public clientAverageCart: String;
  public averageCart: String;
  public averageTotal: String;
  public clientMonthlyTickets: Object;
  public totalBoughtByFamily: Object;
  public totalByFamily: Object;
  public articles: Object;
  public articleRecommendation: Object = null;
  public articleRecommendationReason = null;
  public segmentRecommendation: Object = null;
  public segmentRecommendationReason: {segment, description}[] = null;

  public userProfile: Object;
  public show = false;
  public segmentationIsLoaded = false;
  public userIdError: boolean = true;

  constructor(private statService: UserStatsService, private globalService: GlobalStatsService) { }
  ngOnInit(): void {
    this.articleRecommendation = null;
    this.articleRecommendationReason = null;
    this.segmentRecommendation = null;
    this.segmentRecommendationReason = null;

    this.userIdGroup = new FormGroup({
      userIdFormControl: new FormControl('',
        Validators.required,
        UserIdvalidator.createValidator(this.statService))
    });
    new FormControl('', [
      Validators.required,
      UserIdvalidator.createValidator(this.statService)
    ]);
  }

  loadUserData() {
    this.articleRecommendation = null;
    this.articleRecommendationReason = null;
    this.segmentRecommendation = null;
    this.segmentRecommendationReason = null;
    this.listArticles = null;
    this.checkUserId()
      .then(c => {
        this.userIdError = !c;
        if (this.userIdError) return;

        this.show = true;
        this.segmentationIsLoaded = false;
        this.getTicketAmount();
        this.getAverageTicketAmount();
        this.getClientAverageCartTotal();
        this.getAverageCartTotal();
        this.getClientTotal();
        this.getAverageTotal();
        this.getClientMonthlyTickets();
        this.getNumberBoughtByFamily();
        this.getTotalByFamily();
        this.getArticlesBought();
        this.getUserSegmentRecommandation();
        this.getUserArticleRecommandation();
        // this.getUserProfile();
        if (!this.lineChartData[0].data.length)
          this.getTicketsSegmentation();
      });
  }

  getTicketAmount() {
    this.statService.getTicketAmount(this.userId).subscribe(res => {
      this.totalTickets = res.toLocaleString();
    });
  }

  getAverageTicketAmount() {
    this.statService.getAverageTicketAmount().subscribe(res => this.averageTicketNumber = res.toLocaleString());
  }

  getClientAverageCartTotal() {
    this.statService.getClientAverageCartTotal(this.userId).subscribe(res => {
      this.clientAverageCart = res.toLocaleString()
      var val = Math.ceil(parseFloat(res.toString()))
      if (val > 50) val = 50
      this.lineChartOptions.annotation.annotations[0].value = val.toString()
      this.segmentationIsLoaded = true
    });
  }

  getAverageCartTotal() {
    this.statService.getAverageCartTotal().subscribe(res => {
      this.averageCart = res.toLocaleString()
    });
  }

  getClientTotal() {
    this.statService.getClientTotal(this.userId).subscribe(res => this.clientTotal = res.toLocaleString());
  }

  getAverageTotal() {
    this.statService.getAverageTotal().subscribe(res => this.averageTotal = res.toLocaleString());
  }

  getClientMonthlyTickets() {
    this.statService.getClientMonthlyTickets(this.userId).subscribe(res => this.barChartData[0].data = res);
  }

  getNumberBoughtByFamily() {
    this.statService.getNumberBoughtByFamily(this.userId).subscribe(res => {
      this.pieChartLabels = res.map(o => o.name);
      this.pieChartData[0].backgroundColor = res.map(o => o.color);
      this.pieChartData[0].data = res.map(o => o.value);
    });
  }

  getTotalByFamily() {
    this.statService.getTotalByFamily(this.userId).subscribe(res => {
      this.salesPieChartData[0].backgroundColor = res.map(o => o.color);
      this.salesPieChartLabels = res.map(o => o.perc + "%, " + o.name);
      this.salesPieChartData[0].data = res.map(o => o.value);
    });
  }

  getArticlesBought() {
    this.statService.getArticlesBought(this.userId).subscribe(res => this.listArticles = res);
  }

  getUserSegmentRecommandation() {
    this.statService.getSegmentRecommandation(this.userId).subscribe(res => {
      this.segmentRecommendation = res.recommendation[0]
      this.segmentRecommendationReason = res.raison;
    });
  }

  getUserArticleRecommandation() {
    this.statService.getArticleRecommandation(this.userId).subscribe(res => {
      this.articleRecommendation = res.recommendation[0];      
      this.articleRecommendationReason = res.raisons;
    });
  }

  getUserProfile() {
    this.statService.getUserProfile(this.userId).subscribe(res => this.userProfile = res);
  }

  getTicketsSegmentation() {
    this.globalService.getTicketSegmentation().subscribe(res => {
      this.lineChartData[0].data = res.map(v => v.proportion)
    })
  }

  checkUserId() {
    return this.statService.checkUser(this.userId).toPromise()
  }

  public compareValues(v1, v2) {
    if (v1 === undefined || v2 === undefined) return false

    let f1 = parseFloat(v1.replace(',', '.').replace(/\s+/g, ''));
    let f2 = parseFloat(v2.replace(',', '.').replace(/\s+/g, ''));
    return f1 < f2
  }

}
