# T-DAT-901
## Requirements
```
docker>=19.03.13
docker-compose>=1.26.2
```

## Start docker
First, unzip the database data with `sudo unzip data.zip`.
To start our project, use `docker-compose up -d` and go to [http://localhost:4200](http://localhost:4200) to access the interface.
To access phpmyadmin go to [http://localhost:8183](http://localhost:8183) and login with these credentials :
```
Server: mysql
User: root
Password: root
```
Our API is accessible through [http://localhost:9000](http://localhost:9000).

A hosted version is also accessible at the following URLs:
- Interface: [https://tdat.guillaumebrocard.com/](https://tdat.guillaumebrocard.com/)
- phpmyadmin: [https://admin.tdat.guillaumebrocard.com/](https://admin.tdat.guillaumebrocard.com/)
