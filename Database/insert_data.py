from decimal import Decimal


def insert_all():
    import mysql.connector
    import pandas as pd
    import time
    start_time = time.time()

    db = mysql.connector.connect(
        host="127.0.0.1",
        port=3306,
        user="root",
        password="root",
        database="BigData"
    )

    csv = pd.read_csv("../KaDo.csv", delimiter=",", header='infer')

    families = csv["FAMILLE"].unique()
    universes = csv["UNIVERS"].unique()
    mailles = csv["MAILLE"].unique()

    families_ids = {}
    universes_ids = {}
    mailles_ids = {}

    print("inserting families")
    for i in families:
        cursor = db.cursor()
        sql = "INSERT INTO Families (Name) VALUES (%s)"
        cursor.execute(sql, (i,))
        db.commit()
        families_ids[i] = cursor.lastrowid

    print("inserting universes")
    for i in universes:
        cursor = db.cursor()
        sql = "INSERT INTO Universes (Name) VALUES (%s)"
        cursor.execute(sql, (i,))
        db.commit()
        universes_ids[i] = cursor.lastrowid

    print("inserting mailles")
    for i in mailles:
        cursor = db.cursor()
        sql = "INSERT INTO Mailles (Name) VALUES (%s)"
        cursor.execute(sql, (i,))
        db.commit()
        mailles_ids[i] = cursor.lastrowid

    rows = csv.drop_duplicates(["TICKET_ID"])
    sql = "INSERT INTO Tickets (Id, Month, ClientId) VALUES (%s, %s, %s)"
    data = []
    count = 0
    total_count = 0
    rows_count = rows.count()["TICKET_ID"]
    cursor = db.cursor()
    for i in rows.iterrows():
        if count == 10000 or total_count + 10000 >= rows_count:
            total_count += count
            print(f"{str(total_count)}/{str(rows_count)} tickets")
            try:
                cursor.executemany(sql, data)
                count = 0
                db.commit()
                cursor.close()
                cursor = db.cursor()
                data = []
            except Exception as e:
                print("ticket existe deja", e)

        data.append((i[1]["TICKET_ID"], i[1]["MOIS_VENTE"], i[1]["CLI_ID"]))
        count += 1
    cursor.close()

    count = 0
    total_count = 0
    cursor = db.cursor()
    sql = "INSERT IGNORE INTO Categories (FamilyId, UniverseId, MailleId) VALUES (%s, %s, %s)"
    data = []
    rows = csv.drop_duplicates(["FAMILLE", "UNIVERS", "MAILLE"])
    rows_count = rows.count()["TICKET_ID"]

    for index, row in rows.iterrows():
        if count == 10000 or total_count + 10000 >= rows_count:
            total_count += count
            print(f"{str(total_count)}/{str(rows_count)} categories")
            try:
                cursor.executemany(sql, data)
                count = 0
                db.commit()
                cursor.close()
                cursor = db.cursor()
                data = []
            except Exception as e:
                print("category existe deja", e)

        familyId = families_ids[row["FAMILLE"]]
        universeId = universes_ids[row["UNIVERS"]]
        mailleId = mailles_ids[row["MAILLE"]]
        data.append((familyId, universeId, mailleId))
        count += 1
    cursor.close()
    print(count)

    cursor = db.cursor()
    cursor.execute("SELECT Id, FamilyId, UniverseId, MailleId FROM Categories")
    categories_data = cursor.fetchall()

    categories = {}
    for row in categories_data:
        categories[(row[1], row[2], row[3])] = row[0]

    #select all categories
    cursor = db.cursor()
    cursor.execute("SELECT Name FROM Articles")
    article_names = cursor.fetchall()

    count = 0
    total_count = 0
    cursor.close()
    cursor = db.cursor()
    sql = "INSERT INTO Articles (Name, NetPrice, CategoryId) VALUES (%s, %s, %s)"
    data = []
    rows = csv.drop_duplicates(["LIBELLE", "PRIX_NET"])
    rows_count = rows.count()["TICKET_ID"]
    errors = []
    for index, row in rows.iterrows():
        if count == 10000 or total_count + 10000 >= rows_count:
            total_count += count
            print(f"{str(total_count)}/{str(rows_count)} articles")
            try:
                cursor.executemany(sql, data)
                count = 0
                db.commit()
                cursor.close()
                cursor = db.cursor()
                data = []
            except Exception as e:
                print("article existe deja", e)

        familyId = families_ids[row["FAMILLE"]]
        universeId = universes_ids[row["UNIVERS"]]
        mailleId = mailles_ids[row["MAILLE"]]
        try:
            data.append((row["LIBELLE"], row["PRIX_NET"], categories[(familyId, universeId, mailleId)]))
        except:
            errors.append((familyId, universeId, mailleId))
        count += 1
        # insert category
        # insert article
        # insert ticket
        # insert article_ticket
    cursor.close()

    cursor = db.cursor()
    cursor.execute("SELECT Id, Name, NetPrice FROM Articles")
    article_data = cursor.fetchall()
    articles = {}

    for row in article_data:
        articles[(row[1], row[2])] = row[0]

    # select all categories
    count = 0
    total_count = 0
    cursor.close()
    cursor = db.cursor()
    sql = "INSERT INTO Articles_Tickets (ArticleId, TicketId) VALUES (%s, %s)"
    data = []
    rows = csv.drop_duplicates(["LIBELLE"])
    rows_count = csv.count()["TICKET_ID"]
    for index, row in csv.iterrows():
        if count == 10000 or total_count + 10000 >= rows_count:
            total_count += count
            print(f"{str(total_count)}/{str(rows_count)} articles_tickets")
            try:
                cursor.executemany(sql, data)
                count = 0
                db.commit()
                cursor.close()
                cursor = db.cursor()
                data = []
            except Exception as e:
                print("liaison existe deja", e)

        data.append((articles[(row["LIBELLE"], Decimal(str(row["PRIX_NET"])))], row["TICKET_ID"]))
        count += 1
    cursor.close()
    # select all categories
    # count = 0
    # total_count = 0
    # cursor.close()
    # cursor = db.cursor()
    # sql = "INSERT INTO Articles_Tickets (ArticleId, TicketId) VALUES (%s, %s)"
    # data = []
    # for index, row in csv.iterrows():
    #     if count == 10000 or total_count + 10000 >= rows_count:
    #         total_count += count
    #         print(f"{str(total_count)}/{str(rows_count)} articles_tickets")
    #         try:
    #             cursor.executemany(sql, data)
    #             count = 0
    #             db.commit()
    #             cursor.close()
    #             cursor = db.cursor()
    #             data = []
    #         except Exception as e:
    #             print("liaison existe deja", e)
    #
    #     data.append((articles[(row["LIBELLE"], str(row["PRIX_NET"]))], row["TICKET_ID"]))
    #     count += 1
    #

    # try:
    #     cursor = db.cursor()
    #     sql = "INSERT INTO Articles_Tickets (ArticleId, TicketId) VALUES (%s, %s)"
    #     cursor.execute(sql, (articleId, row["TICKET_ID"]))
    #     db.commit()
    # except Exception  as e:
    #     print("liaison ticket article existe deja", e)
    # total_count += 1
    # if total_count % 1000 == 0:
    #     print(f"{str(total_count)}/{str(rows_count)}")

    print("--- %s seconds ---" % (time.time() - start_time))


if __name__ == "__main__":
    insert_all()
