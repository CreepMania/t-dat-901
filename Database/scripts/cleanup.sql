-- delete articles priced too high or too low
DELETE FROM Articles a
WHERE a.NetPrice > 400
OR a.NetPrice < 0.5
ORDER BY a.NetPrice DESC

-- delete duplicated universes / famillies and mailles
DELETE u2 FROM Universes u1
INNER JOIN Universes u2
WHERE u1.Id < u2.Id
AND u1.Name = u2.Name;

DELETE u2 FROM Families u1
INNER JOIN Families u2
WHERE u1.Id < u2.Id
AND u1.Name = u2.Name;

DELETE u2 FROM Mailles u1
INNER JOIN Mailles u2
WHERE u1.Id < u2.Id
AND u1.Name = u2.Name
