-- List articles
DELIMITER $$
CREATE DEFINER=`root`@`%` PROCEDURE `GetArticleListForClient`(IN `ClientId` INT)
BEGIN
    SELECT a.Name ARTICLE, a.NetPrice PRIX, f.Name FAMILLE, m.Name MAILLE, u.Name UNIVERS, COUNT(a.Id) as QUANTITE
    FROM Tickets t
    INNER JOIN Articles_Tickets a_t 
    	ON (a_t.TicketId = t.Id)
    INNER JOIN Articles a 
    	ON (a.Id = a_t.ArticleId)
    INNER JOIN Categories c 
    	ON (c.Id = a.CategoryId)
    INNER JOIN Families f 
    	ON (f.Id = c.FamilyId)
    INNER JOIN Mailles m 
    	ON (m.Id = c.MailleId)
    INNER JOIN Universes u 
    	ON (u.Id = c.UniverseId)
    WHERE t.ClientId = ClientId
    GROUP BY a.Id
    ORDER BY QUANTITE DESC, a.Name
END$$
DELIMITER ;

-- Panier moyen
DELIMITER $$
CREATE DEFINER=`root`@`%` PROCEDURE `GetAvgCartForClient`(IN `ClientId` INT)
BEGIN
	SELECT AVG(Cart.Price)
    FROM (
        SELECT SUM(a.NetPrice) AS Price
        FROM Tickets t
        INNER JOIN Articles_Tickets a_t
        	ON (t.Id = a_t.TicketId)
        INNER JOIN Articles a
        	ON (a.Id = a_t.ArticleId)
        WHERE t.ClientId = ClientId
        GROUP BY t.Id
    ) Cart;
END$$
DELIMITER ;

-- Panier median
DELIMITER $$
CREATE DEFINER=`root`@`%` PROCEDURE `GetMedianCartForClient`(ClientId INT)
BEGIN
	SELECT Cart.Price
    FROM (
        SELECT SUM(a.NetPrice) AS Price
        FROM Tickets t
        INNER JOIN Articles_Tickets a_t ON (t.Id = a_t.TicketId)
        INNER JOIN Articles a ON (a.Id = a_t.ArticleId)
        WHERE t.ClientId = ClientId
        GROUP BY t.Id
    ) AS Cart,
    (
        SELECT SUM(a.NetPrice) AS Price
        FROM Tickets t
        INNER JOIN Articles_Tickets a_t ON (t.Id = a_t.TicketId)
        INNER JOIN Articles a ON (a.Id = a_t.ArticleId)
        WHERE t.ClientId = ClientId
        GROUP BY t.Id
    ) AS Cart2
    GROUP BY Cart.Price
    HAVING SUM(SIGN(1 - SIGN(Cart.Price - Cart2.Price))) / COUNT(*) > .5
    LIMIT 1;
END$$
DELIMITER ;

-- Frequence d'achat
DELIMITER $$
CREATE DEFINER=`root`@`%` PROCEDURE `GetBuyingFrequencyForClient`(ClientId INT)
BEGIN
	SELECT
	-- frequence
    (COUNT(t.Month) / (SELECT COUNT(t.Id)
                       FROM Tickets t
                       WHERE t.ClientId = ClientId)) * 100
    AS `Frequence d'achat`,
	t.Month AS Mois
    FROM Tickets t
    WHERE t.ClientId = ClientId
    GROUP BY t.Month;
END$$
DELIMITER ;

-- Proportion d'articles achetes
DELIMITER $$
CREATE DEFINER=`root`@`%` PROCEDURE `GetPropotionOfArticlesBoughtForClient`(ClientId INT)
BEGIN
	SELECT
        COUNT(a.Id) AS `Qte`,
        -- proportion en qte
        (COUNT(a.Id) / (SELECT COUNT(a.Id)
                       FROM Tickets t
                       INNER JOIN Articles_Tickets a_t
                            ON t.Id = a_t.TicketId
                       INNER JOIN Articles a
                            ON a_t.ArticleId = a.Id
                       WHERE t.ClientId = ClientId)) * 100 AS `Proportion en qte`,
        SUM(a.NetPrice) AS `Prix`,
        -- proportion en prix
        (SUM(a.NetPrice) / (SELECT SUM(a.NetPrice)
                           FROM Tickets t
                           INNER JOIN Articles_Tickets a_t
                                ON t.Id = a_t.TicketId
                           INNER JOIN Articles a
                                ON a_t.ArticleId = a.Id
                           WHERE t.ClientId = ClientId)) * 100 AS `Proportion en prix`,
        f.Name AS `Famille`
    FROM Tickets t
    INNER JOIN Articles_Tickets a_t
        ON t.Id = a_t.TicketId
    INNER JOIN Articles a
        ON a_t.ArticleId = a.Id
    INNER JOIN Categories c
        ON c.Id = a.CategoryId
    INNER JOIN Families f
        ON c.FamilyId = f.Id
    WHERE t.ClientId = ClientId
    GROUP BY f.Id
    ORDER BY 2 DESC, 4 DESC;
END$$
DELIMITER ;

-- compare prix des articles achetes par rapport aux reste des client
DELIMITER $$
CREATE DEFINER=`root`@`%` PROCEDURE `GetLowAndHighAvgPurchasingPricesForClient`(IN `p_ClientId` INT)
BEGIN
	SET 	@percentage = 10;

	SELECT 		a.Name 											AS `Name`,
    			a.NetPrice 										AS `Purchasing price`,
                @limitValue:= (SELECT 		CEILING(COUNT(ar.Id) * (@percentage / 100))
                               FROM 		Articles ar
                               INNER JOIN 	Articles_Tickets a_t
                               ON ar.Id = a_t.ArticleId
                               AND 	ar.Name = a.Name),
                GetLowAvgPriceForArticle(a.Name, @limitValue) 	AS `Low 10% avg purchasing price`,
                GetTopAvgPriceForArticle(a.Name, @limitValue) 	AS `Top 10% avg puchasing price`,
                GetTopAvgPriceForArticle(a.Name, 99999999) 	AS `Avg purchasing price`
    FROM 		Tickets t
    INNER JOIN 	Articles_Tickets a_t
    	ON 		t.Id = a_t.TicketId
		AND 	t.ClientId = p_ClientId
    INNER JOIN 	Articles a
    	ON 		a.Id = a_t.ArticleId;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`%` PROCEDURE `GetClientIdsForArticleName`(IN ArticleName VARCHAR)
BEGIN
	SELECT DISTINCT t.ClientId
    FROM Articles a
    INNER JOIN Articles_Tickets a_t
    	ON a.Name = ArticleName AND a.Id = a_t.ArticleId
    INNER JOIN Tickets t 
    	ON t.Id = a_t.TicketId;
END$$
DELIMITER ;
