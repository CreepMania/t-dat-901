CREATE TABLE Articles (
    Id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    Name VARCHAR(1000),
    NetPrice DECIMAL(10,2),
    CategoryId INT NOT NULL
);

CREATE TABLE Tickets (
    Id INT NOT NULL PRIMARY KEY,
    Month INT NOT NULL,
    ClientId INT NOT NULL
);

CREATE TABLE Categories (
    Id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    FamilyId INT ,
    UniverseId INT,
    MailleId INT
);

CREATE TABLE Families (
    Id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    Name VARCHAR(1000) NOT NULL
);

CREATE TABLE Universes (
    Id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    Name VARCHAR(1000) NOT NULL
);

CREATE TABLE Mailles (
    Id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    Name VARCHAR(1000) NOT NULL
);

CREATE TABLE Articles_Tickets (
    Id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    ArticleId INT NOT NULL,
    TicketId INT NOT NULL
);

ALTER TABLE Articles_Tickets
ADD CONSTRAINT FK_Articles_Tickets_ArticleId
FOREIGN KEY (ArticleId) REFERENCES Articles(Id);

ALTER TABLE Articles_Tickets
ADD CONSTRAINT FK_Articles_Tickets_TicketId
FOREIGN KEY (TicketId) REFERENCES Tickets(Id);

ALTER TABLE Articles
ADD CONSTRAINT FK_Articles_CategoryId
FOREIGN KEY (CategoryId) REFERENCES Categories(Id);

ALTER TABLE Categories
ADD CONSTRAINT UK_Family_Universe_Maille
UNIQUE (FamilyId, UniverseId, MailleId);

ALTER TABLE Categories
ADD CONSTRAINT FK_Categories_FamilyId
FOREIGN  KEY (FamilyId) REFERENCES Families(Id);

ALTER TABLE Categories
ADD CONSTRAINT FK_Categories_UniverseId
FOREIGN  KEY (UniverseId) REFERENCES Universes(Id);

ALTER TABLE Categories
ADD CONSTRAINT FK_Categories_MailleId
FOREIGN  KEY (MailleId) REFERENCES Mailles(Id);

ALTER TABLE Tickets ADD INDEX IDX_ClientID (ClientId);
