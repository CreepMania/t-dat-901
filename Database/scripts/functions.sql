-- get prix moyen haut d'un article
DELIMITER $$
CREATE DEFINER=`root`@`%` FUNCTION `GetTopAvgPriceForArticle`(`p_Name` VARCHAR(1000), `p_Limit` INT) RETURNS decimal(10,2)
    READS SQL DATA
    DETERMINISTIC
BEGIN

	SELECT 		AVG(a.NetPrice)
    INTO 		@result
    FROM 		Articles a
    INNER JOIN 	Articles_Tickets a_t
    	ON 		a.Id = a_t.ArticleId
    WHERE 		a.Name = p_Name
    ORDER BY 	a.NetPrice DESC
    LIMIT 		p_Limit;

    RETURN @result;

END$$
DELIMITER ;

-- get prix moyen bas d'un article
DELIMITER $$
CREATE DEFINER=`root`@`%` FUNCTION `GetLowAvgPriceForArticle`(`p_Name` VARCHAR(1000), `p_Limit` INT) RETURNS decimal(10,2)
    READS SQL DATA
    DETERMINISTIC
BEGIN

	SELECT 		AVG(Prices.NetPrice)
    INTO 		@result
    FROM (
        SELECT		a.NetPrice
        FROM 		Articles a
        INNER JOIN 	Articles_Tickets a_t
            ON 		a.Id = a_t.ArticleId
        WHERE 		a.Name = p_Name
        ORDER BY 	a.NetPrice
        LIMIT 		p_Limit
    )Prices;

    RETURN @result;

END$$
DELIMITER ;
