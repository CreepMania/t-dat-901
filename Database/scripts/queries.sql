DELIMITER $$

CREATE PROCEDURE SumSpentForClient(ClientId INT)
BEGIN

END$$
DELIMITER ;
-- select all articles from a specific user

SELECT a.Name ARTICLE, a.NetPrice PRIX, f.Name FAMILLE, m.Name MAILLE, u.Name UNIVERS, t.Month MOIS
FROM Tickets t
INNER JOIN Articles_Tickets a_t ON (a_t.TicketId = t.Id)
INNER JOIN Articles a ON (a.Id = a_t.ArticleId)
INNER JOIN Categories c ON (c.Id = a.CategoryId)
INNER JOIN Families f ON (f.Id = c.FamilyId)
INNER JOIN Mailles m ON (m.Id = c.MailleId)
INNER JOIN Universes u ON (u.Id = c.UniverseId)
WHERE t.ClientId = 987698301

-- sum of all the tickets for one user
SELECT SUM(a.NetPrice) 'Prix par ticket'
FROM Tickets t
INNER JOIN Articles_Tickets a_t ON t.Id = a_t.TicketId AND t.ClientId = 987698301
INNER JOIN Articles a ON a.Id = a_t.ArticleId
GROUP BY t.Id

-- nb articles et total debourse par client
SELECT t.ClientId 'Client', COUNT(a.Id) as 'Nb_articles', SUM(a.NetPrice) 'Total'
FROM Tickets t
INNER JOIN Articles_Tickets a_t
	ON t.Id = a_t.TicketId
INNER JOIN Articles a
	ON a_t.ArticleId = a.Id
GROUP BY t.ClientId

-- nb articles par famille
SELECT f.Name, COUNT(f.Id)
FROM Articles a
INNER JOIN Categories c ON c.Id = a.CategoryId
INNER JOIN Families f on c.FamilyId = f.Id
GROUP BY f.Id
ORDER BY 2 DESC

-- nb articles achetes par famille
SELECT f.Name, COUNT(f.Id)
FROM Articles a
INNER JOIN Articles_Tickets a_t ON a.Id = a_t.ArticleId
INNER JOIN Categories c ON c.Id = a.CategoryId
INNER JOIN Families f on c.FamilyId = f.Id
GROUP BY f.Id
ORDER BY 2 DESC

-- nb articles achetes par maille
SELECT m.Name, COUNT(m.Id)
FROM Articles a
INNER JOIN Articles_Tickets a_t ON a.Id = a_t.ArticleId
INNER JOIN Categories c ON c.Id = a.CategoryId
INNER JOIN Mailles m ON m.Id = c.MailleId
GROUP BY m.Id
ORDER BY 2 DESC

--nb articles achetes par univers
SELECT u.Name, COUNT(u.Id)
FROM Articles a
INNER JOIN Articles_Tickets a_t ON a.Id = a_t.ArticleId
INNER JOIN Categories c ON c.Id = a.CategoryId
INNER JOIN Universes u ON c.UniverseId = u.Id
GROUP BY u.Id
ORDER BY 2 DESC

-- nb articles achetes par categorie
SELECT f.Name Family, u.Name Universe, m.Name Maille, COUNT(m.Id) Bought
FROM Articles a
INNER JOIN Articles_Tickets a_t ON a.Id = a_t.ArticleId
INNER JOIN Categories c ON c.Id = a.CategoryId
INNER JOIN Mailles m ON m.Id = c.MailleId
INNER JOIN Universes u ON u.Id = c.UniverseId
INNER JOIN Families f ON f.Id = c.FamilyId
GROUP BY c.Id
ORDER BY 4 DESC

-- select number of articles sold by family
SELECT a.Name, f.Name, COUNT(a.Id)
FROM Families f
INNER JOIN Categories c ON f.Id = c.FamilyId
INNER JOIN Articles a ON c.Id = a.CategoryId
INNER JOIN Articles_Tickets a_t ON a.Id = a_t.ArticleId
GROUP BY a.Name, f.Name

-- selection top 5 items les plus vendus par famille
SELECT Article, Family, Nb
FROM (
    SELECT a.Name Article, f.Name Family, COUNT(a.Id) Nb, (row_number() over (PARTITION BY f.Id order by COUNT(a.Id) DESC)) NbRows
    FROM Families f
    INNER JOIN Categories c ON f.Id = c.FamilyId
    INNER JOIN Articles a ON c.Id = a.CategoryId
    INNER JOIN Articles_Tickets a_t ON a.Id = a_t.ArticleId
    GROUP BY Article, f.Id
) T
WHERE T.NbRows <= 5

-- select articles priced too high or too low
SELECT a.Id, a.Name, a.NetPrice
FROM Articles a
WHERE a.NetPrice > 400
OR a.NetPrice < 0.5
ORDER BY a.NetPrice DESC

-- prix moyen panier general = 15.816655
SELECT AVG(Cart.Price)
FROM (
    SELECT SUM(a.NetPrice) AS Price
    FROM Articles_Tickets a_t
    INNER JOIN Articles a ON (a.Id = a_t.ArticleId)
    GROUP BY a_t.TicketId
) Cart

-- prix moyen du client = 8.580000
SELECT AVG(Cart.Price)
FROM (
    SELECT SUM(a.NetPrice) AS Price
    FROM Tickets t
    INNER JOIN Articles_Tickets a_t ON (t.Id = a_t.TicketId)
    INNER JOIN Articles a ON (a.Id = a_t.ArticleId)
    WHERE t.ClientId = 987698301
    GROUP BY t.Id
) Cart

-- prix panier median pour un client = 4.95
SELECT Cart.Price
FROM (
    SELECT SUM(a.NetPrice) AS Price
    FROM Tickets t
    INNER JOIN Articles_Tickets a_t ON (t.Id = a_t.TicketId)
    INNER JOIN Articles a ON (a.Id = a_t.ArticleId)
    WHERE t.ClientId = 987698301
    GROUP BY t.Id
) AS Cart,
(
    SELECT SUM(a.NetPrice) AS Price
    FROM Tickets t
    INNER JOIN Articles_Tickets a_t ON (t.Id = a_t.TicketId)
    INNER JOIN Articles a ON (a.Id = a_t.ArticleId)
    WHERE t.ClientId = 987698301
    GROUP BY t.Id
) AS Cart2
GROUP BY Cart.Price
HAVING SUM(SIGN(1 - SIGN(Cart.Price - Cart2.Price))) / COUNT(*) > .5
LIMIT 1

-- panier median
SELECT Cart.Price
FROM (
    SELECT SUM(a.NetPrice) AS Price, @clientId := t.ClientId
    FROM Tickets t
    INNER JOIN Articles_Tickets a_t ON (t.Id = a_t.TicketId)
    INNER JOIN Articles a ON (a.Id = a_t.ArticleId)
    GROUP BY t.Id
) AS Cart,
(
    SELECT SUM(a.NetPrice) AS Price
    FROM Tickets t
    INNER JOIN Articles_Tickets a_t ON (t.Id = a_t.TicketId)
    INNER JOIN Articles a ON (a.Id = a_t.ArticleId)
    WHERE t.ClientId = @clientId
    GROUP BY t.Id
) AS Cart2
GROUP BY Cart.Price
HAVING SUM(SIGN(1-SIGN(Cart.Price - Cart2.Price)))/COUNT(*) > .5
LIMIT 1


-- Frequence d'achat par mois pour un client
SELECT
	-- frequence
	(COUNT(t.Month) / (SELECT COUNT(t.Id)
                      FROM Tickets t
                      WHERE t.ClientId = 987698301)) * 100
    AS `Frequence d'achat`,
    t.Month AS Mois
FROM Tickets t
WHERE t.ClientId = 987698301
GROUP BY t.Month

-- Frequence d'achats par mois
SELECT (COUNT(t.Month) / (SELECT COUNT(t.Id) FROM Tickets t)) * 100 AS `Frequence d'achat`, t.Month AS Mois
FROM Tickets t
GROUP BY t.Month

-- proportion en qte + prix des articles achetes par un client
SELECT
	COUNT(a.Id) AS `Qte`,
	-- proportion en qte
	(COUNT(a.Id) / (SELECT COUNT(a.Id)
                   FROM Tickets t
                   INNER JOIN Articles_Tickets a_t
                   		ON t.Id = a_t.TicketId
                   INNER JOIN Articles a
                   		ON a_t.ArticleId = a.Id
                   WHERE t.ClientId = 987698301)) * 100 AS `Proportion en qte`,
	SUM(a.NetPrice) AS `Prix`,
	-- proportion en prix
	(SUM(a.NetPrice) / (SELECT SUM(a.NetPrice)
                       FROM Tickets t
                       INNER JOIN Articles_Tickets a_t
                       		ON t.Id = a_t.TicketId
                       INNER JOIN Articles a
                       		ON a_t.ArticleId = a.Id
                       WHERE t.ClientId = 987698301)) * 100 AS `Proportion en prix`,
	f.Name AS `Famille`
FROM Tickets t
INNER JOIN Articles_Tickets a_t
	ON t.Id = a_t.TicketId
INNER JOIN Articles a
	ON a_t.ArticleId = a.Id
INNER JOIN Categories c
	ON c.Id = a.CategoryId
INNER JOIN Families f
	ON c.FamilyId = f.Id
WHERE t.ClientId = 987698301
GROUP BY f.Id
ORDER BY 2 DESC, 4 DESC

-- répartition du cout des tickets
SELECT ((COUNT(*) / 2734840) * 100), DataTable.PriceRange
FROM (
    SELECT CASE
    	WHEN SUM(a.NetPrice) BETWEEN 0 AND 5 THEN '0-5'
    	WHEN SUM(a.NetPrice) BETWEEN 6 AND 10 THEN '6-10'
    	WHEN SUM(a.NetPrice) BETWEEN 11 AND 15 THEN '11-15'
    	WHEN SUM(a.NetPrice) BETWEEN 16 AND 20 THEN '16-20'
    	WHEN SUM(a.NetPrice) BETWEEN 21 AND 30 THEN '21-30'
    	WHEN SUM(a.NetPrice) BETWEEN 31 AND 50 THEN '31-50'
    	ELSE '50+' END AS PriceRange
    FROM Articles a
    INNER JOIN Articles_Tickets a_t
    ON a.Id = a_t.ArticleId
    GROUP BY a_t.TicketId
)DataTable
GROUP BY DataTable.PriceRange
ORDER BY DataTable.PriceRange


SELECT ((COUNT(*) / 2734840) * 100) AS Proportion, DataTable.PriceRange
FROM (
    SELECT IF(CEILING(SUM(a.NetPrice)) >= 50, 50, CEILING(SUM(a.NetPrice))) AS PriceRange
    FROM Articles a
    INNER JOIN Articles_Tickets a_t
    ON a.Id = a_t.ArticleId
    GROUP BY a_t.TicketId
)DataTable
GROUP BY DataTable.PriceRange
HAVING Proportion > 0.001
ORDER BY DataTable.PriceRange